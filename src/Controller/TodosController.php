<?php
namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TodosController extends AbstractController {

    public function list(Request $request,$page) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $limitForPage = 10;
        $todos = $this
            ->getDoctrine()
            ->getRepository(Todo::class)
            ->paginateByUser($page, $limitForPage, $this->getUser());

        $count = count($todos);
        return $this->render(
            'list.html.twig',
            [
                'count' => $count,
                'todos' => $todos,
                'pages' => range(1, ceil($count / $limitForPage)),
                'currentPage' => $page
            ]
        );
    }

    public function newTodo(Request $request) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $todo = new Todo();
        $todo->setText('');
        $todo->setIsDone(false);

        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $todo->setUser($this->getUser());
            $entManager = $this
                ->getDoctrine()
                ->getManager();
            $entManager->persist($todo);
            $entManager->flush();
            return $this->redirect('/');
        }

        return $this->render(
            'edit.html.twig',
            [
                'form' => $form->createView(),
            ]);
    }

    public function editTodo($id, Request $request) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $todo = $this
            ->getDoctrine()
            ->getRepository(Todo::class)
            ->find($id);

        if(!isset($todo)) {
            return $this->redirect($request->headers->get('referer'));
        }

        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $todo = $form->getData();
            $todo->setUser($this->getUser());
            $entManager = $this
                ->getDoctrine()
                ->getManager();
            $entManager->persist($todo);
            $entManager->flush();
            return $this->redirect('/');
        }

        return $this->render(
            'edit.html.twig',
            [
                'form' => $form->createView(),
            ]);
    }

    public function deleteTodo($id, Request $request) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entManager = $this
            ->getDoctrine()
            ->getManager();

        $todo = $this
            ->getDoctrine()
            ->getRepository(Todo::class)
            ->find($id);

        $referer = $request->headers->get('referer');

        if(isset($todo)) {
            $entManager->remove($todo);
            $entManager->flush();
        }

        return $this->redirect($referer);
    }

    public function toggleIsDone($id, Request $request) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entManager = $this
            ->getDoctrine()
            ->getManager();

        $todo = $this
            ->getDoctrine()
            ->getRepository(Todo::class)
            ->find($id);

        $referer = $request->headers->get('referer');

        if(isset($todo)) {
            $todo->setIsDone(!$todo->getIsDone());
            $entManager->persist($todo);
            $entManager->flush();
            return $this->redirect($referer);
        } else {
            return $this->redirect($referer);
        }
    }

}