<?php

namespace App\Repository;

use App\Entity\Todo;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Todo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todo[]    findAll()
 * @method Todo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Todo::class);
    }

    public function paginate($page, $limit) {
        $offset = ($page - 1) * $limit;
        $entManager = $this->getEntityManager();

        $query = $entManager->createQueryBuilder()
            ->select('t')
            ->from('App\Entity\Todo', 't')
            ->orderBy('t.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $paginator = new Paginator($query);

        return $paginator;
    }

    public function paginateByUser($page, $limit, User $user) {
        $offset = ($page - 1) * $limit;
        $entManager = $this->getEntityManager();

        $query = $entManager->createQueryBuilder()
            ->select('t')
            ->from('App\Entity\Todo', 't')
            ->where('t.userId = :uid')
            ->orderBy('t.id', 'DESC')
            ->setParameter('uid', $user->getId())
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $paginator = new Paginator($query);

        return $paginator;
    }
    // /**
    //  * @return Todo[] Returns an array of Todo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Todo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
