<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestConsoleCommand extends Command {

    public function __construct($name = 'test-command')
    {
        parent::__construct($name);
    }

    public function configure() {
        $this
            ->setName('app:test-command')
            ->setDescription('Any string passed to this command will be returned reversed.')
            ->addArgument('text', InputArgument::OPTIONAL, 'This text will be reversed', '');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output
            ->writeln(strrev($input->getArgument('text')));
    }
}